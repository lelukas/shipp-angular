import {AfterViewInit, Component} from '@angular/core';
import {Router} from '@angular/router';
import {AddressService} from './services/address.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements  AfterViewInit {
  title = 'Shipp';
  constructor(private router: Router, private addressService: AddressService) {}

  ngAfterViewInit(): void {
    if (!this.addressService.origin) {
      this.router.navigateByUrl('');
    }
  }
}
