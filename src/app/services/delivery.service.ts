import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DeliveryService {
  readonly baseUrl = 'https://bzevgcar2b.execute-api.sa-east-1.amazonaws.com';
  constructor(
    private http: HttpClient
  ) {}

  async getDeliveryPriceAndDistance(waypointsArr, description) {
    const params = {
      stops: waypointsArr,
      description
    };
    try {
      return this.http.post(`${this.baseUrl}/evaluate`, params).toPromise();
    } catch (e) {
      return e;
    }
  }

  async orderDelivery(creditCard, waypoints, description) {
    const params = {
      ...creditCard,
      stops: waypoints,
      description
    };
    try {
      return this.http.post(`${this.baseUrl}/service`, params).toPromise();
    } catch (e) {
      return e;
    }
  }
}
