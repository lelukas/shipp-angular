import { Component, OnInit } from '@angular/core';
import { AddressService } from '../../services/address.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit {
  lat = -14.235004;
  lng = -51.92527999999999;
  originZoom = 18;

  constructor(private addresses: AddressService) {}

  get origin() {
    return this.addresses.origin;
  }

  get destination() {
    return this.addresses.destination;
  }

  get waypoints() {
    return this.addresses.waypoints;
  }

  ngOnInit() {}

}
