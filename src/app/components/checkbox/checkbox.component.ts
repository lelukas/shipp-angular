import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.sass']
})
export class CheckboxComponent implements OnInit {
  @Input() value: boolean;
  @Input() name: string;
  @Input() label: string;
  @Input() style: string;
  @Output() check = new EventEmitter<boolean>();

  constructor() { }
  emitValue({ target }) {
    this.check.emit(target.checked);
  }
  ngOnInit() {
  }

}
