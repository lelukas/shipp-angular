import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { card } from 'creditcards';
import { PaymentService } from '../../services/payment.service';

@Component({
  selector: 'app-new-credit-card',
  templateUrl: './new-credit-card.component.html',
  styleUrls: ['./new-credit-card.component.sass']
})
export class NewCreditCardComponent implements OnInit {
  creditCardForm: any;
  cardFlag: string;
  constructor(
    private router: Router,
    private paymentService: PaymentService
  ) { }

  ngOnInit() {
    this.creditCardForm = new FormGroup({
      number: new FormControl(),
      name: new FormControl(),
      expDate: new FormControl(),
      cvv: new FormControl(),
    });
  }

  goTo(route: string) {
    this.router.navigateByUrl(route);
  }

  isInputInvalid(inputName) {
    const isInvalid = this.creditCardForm.controls[inputName].invalid;
    const wasTouched = this.creditCardForm.controls[inputName].touched;
    return isInvalid && wasTouched;
  }

  checkCardFlag() {
    const cardNumber = this.creditCardForm.controls.number.value;
    const cardFlag = card.type(cardNumber, true);
    if (cardFlag) {
      this.cardFlag = cardFlag.replace(' ', '-').toLowerCase();
    }
  }

  addCreditCard() {
    if (this.creditCardForm.valid) {
      const creditCard = {
        name: this.creditCardForm.get('name').value,
        card_number: this.creditCardForm.get('number').value,
        expiry_date: this.creditCardForm.get('expDate').value,
        cvv: this.creditCardForm.get('cvv').value
      };
      this.paymentService.addCreditCard(creditCard);
      this.router.navigateByUrl('deliveryman');
    }
  }
}
