import { Component, OnInit } from '@angular/core';
import { AddressService } from '../../services/address.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-address',
  templateUrl: './new-address.component.html',
  styleUrls: ['./new-address.component.sass']
})
export class NewAddressComponent implements OnInit {
  // hasComplement: boolean;
  complement: string;
  selectedAddress: any;
  noAddressError: boolean;
  addressToEdit: any;

  constructor(private addressService: AddressService, private router: Router) {
    // this.hasComplement = true;
    this.selectedAddress = {};
    this.complement = '';
    this.noAddressError = false;
  }

  ngOnInit() {
    if (history.state.data) {
      const { selectedAddressIndex } = history.state.data;
      this.selectedAddress = this.addressService.addresses[selectedAddressIndex];
      this.addressToEdit = this.addressService.addresses[selectedAddressIndex].formatted_address;
      this.complement = this.addressService.addresses[selectedAddressIndex].complement;
    }
  }

  // getValue(hasComplement) {
  //   console.log(hasComplement)
  //   if (!hasComplement) { this.complement = ''; }
  //   this.hasComplement = !hasComplement;
  // }

  saveAddress(address) {
    this.selectedAddress = address;
  }

  add() {
    if (Object.entries(this.selectedAddress).length) {
      this.selectedAddress.complement = this.complement;
      this.noAddressError = false;
      this.addressService.addAddress(this.selectedAddress);
      this.router.navigateByUrl('deliveryman');
    } else {
      this.noAddressError = true;
    }
  }

  editAddress() {
    const { source } = history.state.data;
    this.selectedAddress.complement = this.complement;
    if (source === 'origin') {
      this.addressService.updateOrigin(this.selectedAddress, true);
    } else if (source === 'destination') {
      this.addressService.updateDestination(this.selectedAddress, true);
    } else {
      const { selectedAddressIndex } = history.state.data;
      this.addressService.updateWaypoint(this.selectedAddress, selectedAddressIndex);
    }
    this.goTo('deliveryman');
  }

  goTo(route: string) {
    this.router.navigateByUrl(route);
  }
}
