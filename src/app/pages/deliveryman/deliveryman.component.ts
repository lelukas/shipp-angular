import { Component, AfterViewInit } from '@angular/core';
import { AddressService } from '../../services/address.service';
import { DeliveryService } from '../../services/delivery.service';
import { PaymentService, CreditCard } from '../../services/payment.service';
import { faTimes, faPen } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { card } from 'creditcards';

@Component({
  selector: 'app-deliveryman',
  templateUrl: './deliveryman.component.html',
  styleUrls: ['./deliveryman.component.sass']
})
export class DeliverymanComponent implements AfterViewInit {
  alphabet = ['A', 'B', 'C', 'D', 'E', 'F'];
  faPen = faPen;
  faTimes = faTimes;
  selectedCreditCard: CreditCard;
  noPaymentSelectedError = false;
  notEnoughStopsError = false;
  description = '';
  deliveryData = {
    distance: undefined,
    total_value: undefined
  };

  constructor(
    private addressesService: AddressService,
    private deliveryService: DeliveryService,
    private paymentService: PaymentService,
    private router: Router
  ) {}

  async ngAfterViewInit() {
    this.updateDeliveryPriceAndDistance();
  }

  async updateDeliveryPriceAndDistance() {
    if (this.addressesService.origin) {
      this.deliveryData = await this.deliveryService.getDeliveryPriceAndDistance(this.extractDeliveryStops(), '');
    }
  }

  extractDeliveryStops() {
    const coordsArr = [];
    const origin = this.addressesService.origin;
    const destination = this.addressesService.destination;
    const waypoints = this.addressesService.waypoints;
    coordsArr.push(this.assembleCoordsEdge(origin));
    if (destination) {
      coordsArr.push(this.assembleCoordsEdge(destination));
      if (waypoints.length) {
        waypoints.forEach(({ location }) => {
          coordsArr.push({
            latitude: location.lat,
            longitude: location.lng
          });
        });
      }
    }
    return coordsArr;
  }

  assembleCoordsEdge(edge) {
    return { latitude: edge.lat, longitude: edge.lng };
  }

  get addresses() {
    return this.addressesService.addresses;
  }

  get creditCards() {
    return this.paymentService.creditCards;
  }

  get formattedDistance() {
    if (this.deliveryData.distance && this.addressesService.destination) {
      const distance = this.deliveryData.distance.toFixed(2);
      return `${distance.replace('.', ',')} Km`;
    }
    return '-';
  }

  get formattedPrice() {
    if (this.deliveryData.total_value && this.addressesService.destination) {
      const value = this.deliveryData.total_value.toFixed(2);
      return `R$ ${value.replace('.', ',')}`;
    }
    return '-';
  }

  getCreditCardImage(cardNumber) {
    const cardFlag = card.type(cardNumber, false);
    if (cardFlag) {
      return cardFlag.replace(' ', '-').toLowerCase();
    }
  }

  hideCreditCardNumber(cardNumber) {
    const lastDigits = cardNumber.substring(cardNumber.length - 4);
    return '•••• •••• •••• ' + lastDigits;
  }

  renderErrorMessage() {
    if (this.noPaymentSelectedError) { return 'Selecione um método de pagamento'; }
    if (this.notEnoughStopsError) { return 'Selecione ao menos dois pontos de parada'; }
  }

  selectPayment(index) {
    this.noPaymentSelectedError = false;
    this.selectedCreditCard = this.paymentService.creditCards[index];
  }

  getSource(selectedItemIndex): string {
    const addressesLength = this.addresses.length;
    let source;
    if (selectedItemIndex === 0) {
      source = 'origin';
    } else if (selectedItemIndex === addressesLength - 1 && !!this.addressesService.destination) {
      source = 'destination';
    } else {
      source = 'waypoints';
    }
    return source;
  }

  editStop(index) {
    const source = this.getSource(index);
    this.goTo('', {source, selectedAddressIndex: index});
  }

  removeStop(index) {
    this.addressesService.removeStop(index);
    this.updateDeliveryPriceAndDistance();
  }

  async requestDeliveryman() {
    if (!this.selectedCreditCard) { return this.noPaymentSelectedError = true; }
    if (!this.deliveryData.total_value) { return this.notEnoughStopsError = true; }
    this.notEnoughStopsError = false;
    // const response = await this.deliveryService.orderDelivery(this.selectedCreditCard, this.extractDeliveryStops(), this.description);
    // console.log(response);
    this.goTo('order-complete', { data: 'asnaeb' });
  }

  removeCreditCard(index) {
    this.paymentService.removeCreditCard(index);
  }

  goTo(route: string, data?: any) {
    this.router.navigateByUrl(route, { state: { data } });
  }
}
