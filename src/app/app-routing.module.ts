import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliverymanComponent } from './pages/deliveryman/deliveryman.component';
import { NewAddressComponent } from './pages/new-address/new-address.component';
import { NewCreditCardComponent } from './pages/new-credit-card/new-credit-card.component';
import { OrderCompleteComponent } from './pages/order-complete/order-complete.component';

const routes: Routes = [
  { path: '', component: NewAddressComponent },
  { path: 'deliveryman', component: DeliverymanComponent },
  { path: 'new-credit-card', component: NewCreditCardComponent },
  { path: 'order-complete', component: OrderCompleteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  DeliverymanComponent,
  NewAddressComponent,
  NewCreditCardComponent,
  OrderCompleteComponent
];
